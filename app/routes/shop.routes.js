//Shop routes

module.exports = app => {
    const shop = require("../controllers/shop.controller.js");

    //Create position
    app.post("/shop", shop.create);

    app.get("/shop", shop.findAll);

    app.get("/shop/:shopItemId",shop.findOne);

    app.patch("/shop/:shopItemId", shop.update);

    app.delete("/shop/:shopItemId", shop.delete);
}