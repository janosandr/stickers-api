// Services

module.exports = app => {
    const service = require("../controllers/services.controller.js");

    app.post("/services/new", service.create);

    app.get("/services", service.findAll);

    app.get("/services/:serviceItemId",service.findOne);
}