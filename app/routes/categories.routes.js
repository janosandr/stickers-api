// Category routes

module.exports = app => {
    const category = require("../controllers/categories.controller.js");

    app.post("/categories", category.create);

    app.get("/categories", category.findAll);

    app.get("/categories/products/:categoryItemId", category.getProdCat);
    
    app.patch("/categories/:categoryItemId", category.update);
    
    app.delete("/categories/:categoryItemId", category.delete);
}