module.exports = app => {
    const slide = require("../controllers/sliders.controller.js");

    app.post("/slides", slide.create);

    app.get("/slides", slide.findAll);

    app.get("/slides/:slideId", slide.findOne);

    app.patch("/slides/:slideId", slide.update);

    app.delete("/slides/:slideId", slide.delete);
}