//Order routes

module.exports = app => {
    //here path to order controller 
    const order = require("../controllers/orders.controller.js");

    // routes

    app.post("/orders/new", order.create);

    app.get("/orders", order.findAll);

    app.get("/orders/:orderId",order.findOne);

    app.patch("/orders/:orderId", order.update);

    app.delete("/orders/:itemId");
}