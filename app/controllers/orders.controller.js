const Order = require("../models/orders.model.js");

exports.create = (req, res) =>{
    if(!req.body){
        res.status(400).send({
            message:"Content can't be empty!"
        });
    }

    const order = new Order({
        name: req.body.name,
        surname: req.body.surname,
        city: req.body.city,
        phone: req.body.phone,
        email: req.body.email,
        // delivery_type: req.body.delivery_type,
        delivery_location: req.body.delivery_location,
        status: req.body.status,
        products:req.body.products,
        total: req.body.total
    });

    Order.create(order, (err,data)=>{
        if(err)
            res.status(500).send({
                message:
                    err.message || "Some error occured while ordering.."
            });

            else res.send(data);
    });
};

exports.findAll = (req,res)=>{
    Order.getAll((err,data)=>{
        if(err)
            res.status(500).send({
                message:
                err.message || "error while retrieving orders"
            });
            else res.send(data);
    });
};

exports.findOne = (req, res) => {
    Order.findById(req.params.orderId,(err,data) => {
        if(err){
            if(err.kind === "not_found"){
                res.status(404).send({
                    message: `Not found orders with id ${req.params.orderId}`
                });
            } else {
                res.status(500).send({
                    message: `Error while retrieving order with ID: ${req.params.orderId}`
                });
            }
        } else res.send(data);
    });
};

exports.update = (req,res)=>{
    if(!req.body){
        res.status(400).send({
           message:"Content can't be empty!" 
        });
    }

    Order.updateById(
        req.params.orderId,
        new Order(req.body),
        (err,data) => {
            if(err){
                if(err.kind === "not_found"){
                    res.status(404).send({
                        message:`Not found Order with id ${req.params.orderId}`
                    });
                } else {
                    res.status(500).send({
                        message:`Error updating order with id: ${req.params.orderId}`
                    });
                }
            } else res.send(data);
        }
    );

};

exports.delete = (req,res) => {
    Order.remove(req.params.orderId, (err, data) => {
        if(err){
            if(err.kind === "not_found"){
                res.status(404).send({
                    message: `Not found order with id: ${req.params.orderId}`
                });
            } else {
                res.status(500).send({
                    message: `Couldn't delete order with id ${req.params.orderId}`
                });
            }
        } else res.send({message:"Order succesfully deleted!"});
    });
}