const Service = require("../models/services.model.js")

exports.create = (req,res) => {
    if(!req.body){
        res.status(400).send({
            message: "Content cannot be empty!"
        });
    }

    const service = new Service({
        title: req.body.title,
        logo: req.body.logo,
        description: req.body.description
    });

    Service.create(service, (err,data) => {
        if(err)
        res.status(500).send({
            message:
                err.message || "Some error with creating a new service"
        });
        else res.send(data);
    });
}
// Retrieve all
exports.findAll = (req,res) => {
    Service.getAll((err,data) => {
        if(err)
            res.status(500).send({
                message:
                    err.message || "Some error occured while retrieving products"
            });
            else res.send(data);
    });
};



//Retrieve by ID
exports.findOne = (req,res) => {
    Service.findById(req.params.serviceItemId, (err,data) => {
        if(err){
            if(err.kind === "not_found"){
                res.status(404).send({
                    message: `Not found service with id ${req.params.serviceItemId}.`
                });
            } else {
                res.status(500).send({
                    message: "Error while retrieving service with id " + req.params.serviceItemId
                });
            }
        } else res.send(data);
    });
};