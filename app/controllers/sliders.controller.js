const Slide = require("../models/sliders.model.js");

exports.create = (req,res) => {
    if(!req.body){
        res.status(400).send({
            message: "Content cannotbe empty!"
        });
    }

    const slide = new Slide({
        type: req.body.type,
        title: req.body.title,
        subtitle: req.body.subtitle,
        link: req.body.link,
        link_text: req.body.link_text,
        picture: req.body.picture,
        media: req.body.media,
        styles: req.body.styles
    });

    Slide.create(slide, (err,data) => {
        if(err)
        res.status(500).send({
            message:
                err.message || "Some error occured while creating a new product with method Shop."
    });
    else res.send(data);
    });
}

exports.findAll = (req,res)=>{
    Slide.getAll((err,data)=>{
        if(err)
            res.status(500).send({
                message:
                    err.message || "Some error occured while retrieving products"
            });
            else res.send(data);
    });
};

exports.findOne =(req,res)=>{
    Slide.findById(req.params.slideId, (err,data) => {
        if(err){
            if(err.kind === "not_found"){
                res.status(404).send({
                    message: `Not found product with id ${req.params.slideId}.`
                });
            } else {
                res.status(500).send({
                    message: "Error while retrieving product with id " + req.params.slideId
                });
            }
        } else res.send(data);
    });

};

exports.update = (req,res) => {
    if (!req.body) {
        res.status(400).send({
          message: "Content can not be empty!"
        });
      }

    Slide.updateById(
        req.params.slideId,
        new Slide(req.body),
        (err,data)=>{
          if(err){
              if(err.kind === "not_found"){
                  res.status(404).send({
                      message: `Not found Customer with id ${req.params.shopItemId}.`
                  });
              } else {
                  res.status(500).send({
                      message: "Error updating Customer with id " + req.params.shopItemId
                  });
              }
          } else res.send(data);
        }
    );
}

exports.delete = (req,res) => {
    Slide.remove(req.params.slideId, (err,data) => {
        if(err){
            if(err.kind === "not_found"){
                res.status(404).send({
                    message: `Not found Product with id ${req.params.shopItemId}.`
                });
            } else {
                res.status(500).send({
                    message: "Could not delete Customer with id " + req.params.shopItemId 
                });
            } 
        } else res.send({ message: `Customer was deleted successfully!` });
    });
}