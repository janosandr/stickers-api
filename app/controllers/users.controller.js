const User = require("../models/users.model.js");

exports.create = (req,res) => {
    if(!req.body){
        res.status(400).send({
            message: "Content cannotbe empty!"
        });
    }

    const user = new User({
        username: req.body.username,
        name: req.body.name,
        surname: req.body.surname,
        avatar: req.body.avatar,
        email: req.body.email,
        locale: req.body.locale,
        token: req.body.token,
        
    });

    User.create(user, (err,data)=>{
        if(err)
            res.status(500).send({
                message:
                    err.message || "Some error occured while creating a new user with method User."
        });
        else res.send(data);
    });
};

exports.findAll = (req,res) =>{
    User.getAll((err,data)=>{
        if(err)
        res.status(500).send({
            message:
                err.message || "Some error occured while retrieving users"
        });
        else res.send(data);
    });
};

exports.findOne = (req,res) =>{
    User.findById(req.params.userId, (err,data)=>{
        if(err){
            if(err.kind === "not_found"){
                res.status(404).send({
                    message: `Not found user with id ${req.params.userId}.`
                });
            } else {
                res.status(500).send({
                    message: "Error while retrieving user with id " + req.params.userId
                });
            }
        } else res.send(data);
  });
};