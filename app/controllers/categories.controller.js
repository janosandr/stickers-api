const Category = require("../models/categories.model.js");

exports.create = (req,res) => {
    if(!req.body){
        res.status(400).send({
            message: "Content cannot be empty!"
        });
    }

    const category = new Category({
        title: req.body.title,
        icon: req.body.icon
    });

    Category.create(category, (err,data)=>{
        if(err)
            res.status(500).send({
                message:
                    err.message || "Some error with creating a new category"
            });
            else res.send(data);
    });
};



exports.findAll = (req,res) => {
    Category.getAll((err,data) => {
        if(err)
            res.status(500).send({
                message:
                    err.message || "Some error while get the categories"
            });
            else res.send(data);
    });
};

exports.update = (req,res) => {
    if(!req.body) {
        res.status(400).send({
            message: "Content can't be empty"
        });
    }

    Category.updateById(
        req.params.categoryItemId,
        new Category(req.body),
        (err,data) => {
            if(err){
                if(err.kind === "not_found"){
                    res.status(404).send({
                        message: `Not found with id ${req.params.categoryItemId}`
                    });
                } else {
                    res.status(500).send({
                        message: "Error updating with id " + req.params.categoryItemId
                    });
                }
            } else res.send(data);
        }
    );
};

exports.getProdCat = (req, res) => {
    Category.getProductsById(req.params.categoryItemId, (err,data)=>{
        if(err){
            if(err.kind === "not_found"){
                res.status(404).send({
                    message: `Not found product with id ${req.params.categoryItemId}.`
                });
            } else {
                res.status(500).send({
                    message: "Error while retrieving product with id " + req.params.categoryItemId
                });
            }
        } else res.send(data);
    });
};

exports.delete = (req,res) => {
    Category.remove(req.params.categoryItemId, (err,data) => {
        if(err){
            if(err.kind === "not_found"){
                res.status(404).send({
                    message: `Not found category with id ${req.params.categoryItemId}`
                });
            } else {
                res.status(500).send({
                    message: `Couldn't delete category with id: ${req.params.categoryItemId}`
                });
            }
        } else res.send({message: `Category was deleted succesfully`});
    });
};