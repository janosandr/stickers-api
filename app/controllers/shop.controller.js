const Shop = require("../models/shop.model.js");

// Create and Save
exports.create = (req,res) => {
    //Validate content
    if(!req.body){
        res.status(400).send({
            message: "Content cannotbe empty!"
        });
    }

    //Create a product
    const shop = new Shop({
        title: req.body.title,
        description: req.body.description,
        category_id: req.body.category_id,
        picture: req.body.picture,
        gallery_id: req.body.gallery_id,
        price: req.body.price,
        quantity: req.body.quantity
    });

    //Save
    Shop.create(shop, (err, data)=>{
        if(err)
            res.status(500).send({
                message:
                    err.message || "Some error occured while creating a new product with method Shop."
        });
        else res.send(data);
    });

};

// Retrieve all
exports.findAll = (req,res) => {
    Shop.getAll((err,data) => {
        if(err)
            res.status(500).send({
                message:
                    err.message || "Some error occured while retrieving products"
            });
            else res.send(data);
    });
};


//Retrieve by ID
exports.findOne = (req,res) => {
    Shop.findById(req.params.shopItemId, (err,data) => {
        if(err){
            if(err.kind === "not_found"){
                res.status(404).send({
                    message: `Not found product with id ${req.params.shopItemId}.`
                });
            } else {
                res.status(500).send({
                    message: "Error while retrieving product with id " + req.params.shopItemId
                });
            }
        } else res.send(data);
    });
};

exports.update = (req,res) => {
    if (!req.body) {
        res.status(400).send({
          message: "Content can not be empty!"
        });
      }
  
      Shop.updateById(
        req.params.shopItemId,
          new Shop(req.body),
            (err,data)=>{
                if(err){
                    if(err.kind === "not_found"){
                        res.status(404).send({
                            message: `Not found Customer with id ${req.params.shopItemId}.`
                        });
                    } else {
                        res.status(500).send({
                            message: "Error updating Customer with id " + req.params.shopItemId
                        });
                    }
                } else res.send(data);
            }
      );
};

exports.delete = (req,res) => {
    Shop.remove(req.params.shopItemId, (err,data) => {
        if(err){
            if(err.kind === "not_found"){
                res.status(404).send({
                    message: `Not found Product with id ${req.params.shopItemId}.`
                });
            } else {
                res.status(500).send({
                    message: "Could not delete Customer with id " + req.params.shopItemId 
                });
            } 
        } else res.send({ message: `Customer was deleted successfully!` });
    });
};