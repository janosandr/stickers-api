const sql = require("./db.js");

const Slide = function(slide){
    this.type = slide.type;
    this.title = slide.title;
    this.link = slide.link;
    this.subtitle = slide.subtitle;
    this.link_text = slide.link_text;
    this.picture = slide.picture;
    this.media = slide.media;
    this.styles = slide.styles;
};

Slide.create = (newSlide,result) =>{
    sql.query("INSERT INTO slides SET ?", newSlide, (err,res)=>{
        if(err){
            console.log("Error: ", err);
            result(err,null);
            return;
        }

        console.log("Shop item created: ", {id: res.insertId, ...newSlide});
        result(null, {id: res.insertId, ...newSlide});
    });
};

Slide.getAll = result =>{
    sql.query("SELECT * FROM slides", (err,res) =>{
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
          }
      
          console.log("slides: ", res);
          result(null, res);
    });
}

Slide.findById = (slideId,result) =>{
    sql.query(`SELECT * FROM slides WHERE id = ${slideId}`, (err,res) =>{
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
          }
      
          if (res.length) {
            console.log("found slides item: ", res[0]);
            result(null, res[0]);
            return;
          }
      
          // not found Customer with the id
          result({ kind: "not_found" }, null);
    });
};

Slide.updateById = (id, slide, result) => {
    sql.query("UPDATE slides SET type = ?, title = ?, subtitle = ?, link = ?, link_text = ?, picture = ?, media = ?, styles =? WHERE id = ?",
    [slide.type, slide.title,  slide.subtitle, slide.link, slide.link_text, slide.picture, slide.media, slide.styles, id],
    (err,res)=>{
        if (err){
            console.log("error: ", err);
            result(null,err);
            return;
        }

        if(res.affectedRow == 0){
            result({kind: "not_found"}, null);
            return;
        }

        console.log("updated slide: ", {id:  id, ...slide});
        result(null, {id:id, ...slide});
    }
    );
};

Slide.remove = (id, result) => {
    sql.query("DELETE FROM slides WHERE id = ?", id, (err,res) => {
        if(err){
            console.log("error: ", err);
            result(null, err);
            return;
        }

        if(res.affectedRow == 0){
            result({kind: "not_found"}, null);
            return;
        }

        console.log("deleted slides with id: ", id);
        result(null, res);
    });
}

module.exports = Slide;