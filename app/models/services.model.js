const sql = require("./db.js");

const Service = function(service){
    this.title = service.title;
    this.logo = service.logo;
    this.description = service.description;
};

Service.create = (newService, result) => {
    sql.query("INSERT INTO services SET ?", newService, (err,res) => {
        if(err){
            console.log("Error:", err);
            result(err, null);
            return;
        }

        console.log("New service! It is: ", {id: res.insertId, ...newService})
        result(null,  {id: res.insertId, ...newService})
    });
}

Service.getAll = result => {
    sql.query("SELECT * FROM services", (err,res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
          }
      
          console.log("Service: ", res);
          result(null, res);
    });
}


Service.findById = (serviceItemId, result) => {
    sql.query(`SELECT * FROM services WHERE id = ${serviceItemId}`, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(err, null);
        return;
      }
  
      if (res.length) {
        console.log("found shop item: ", res[0]);
        result(null, res[0]);
        return;
      }
  
      // not found Customer with the id
      result({ kind: "not_found" }, null);
    });
  };

module.exports = Service;