const sql = require("./db.js");

const Order = function(order){
    this.name = order.name;
    this.surname = order.surname;
    this.city = order.city;
    this.phone = order.phone;
    this.email = order.email;
    // this.delivery_type = order.delivery_type;
    this.delivery_location = order.delivery_location;
    this.status = order.status;
    this.products =  order.products;
    this.total = order.total;
    this.created_at = new Date();
};

Order.create = (newOrder, result) => {
    sql.query("INSERT INTO orders SET ?", newOrder,(err,res)=>{
        if(err){
            console.log("Error:", err);
            result(err, null);
            return;
        }

        

        console.log("New order! It is: ", {id: res.insertId, ...newOrder})
        result(null,  {id: res.insertId, ...newOrder})
    });
};

Order.getAll = result => {
    sql.query("SELECT * FROM orders",(err,res)=>{
        if(err){
            console.log("error: ", err);
            result(null,err);
            return;
        }

        console.log("Orders: ", res);
        result(null, res);
    });
};

Order.findById = (orderId,result) => {
    sql.query("SELECT * FROM orders WHERE id = ?",orderId, (err,res)=>{
        if(err){
            console.log("error: ", err);
        result(err, null);
        return;
        }

        if(res.length){
            console.log("found order item: ", res[0]);
            result(null, res[0]);
            return;
        }

        result({kind:"not_found"},null);
    });
}

Order.updateById = (id, order, result) => {
    sql.query("UPDATE orders SET name = ?, surname = ?, city = ?, phone = ?, email = ?, delivery_type = ?, delivery_location = ?, status = ?, quantity = ?, products = ?, total = ? WHERE id = ?",
        [order.name, order.surname, order.city, order.phone, order.email, order.delivery_type, order.delivery_location, order.status, order.quantity, order.products, order.total, id],
        (err,res) => {
            if (err) {
                console.log("error: ", err);
                result(null, err);
                return;
            }
    
            if(res.affectedRow == 0){
                result({kind: "not_found"}, null);
                return;
            }
            
            console.log("Order updated: ",{id: id, ...order});
            result(null, {id:id, ...order});
        }
    );
};

Order.remove = (id, result) => {
    sql.query("DELETE FROM orders WHERE id = ?", id, (err,res) => {
        if(err){
            console.log("err: ", err);
            result(null, err);
            return;
        }

        if(res.affectedRow == 0){
            result({kind: "not_found"}, null);
            return;
        }

        console.log("deleted category with id: ", id);
        result(null,res);
    });
}

module.exports = Order;