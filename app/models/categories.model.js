const sql = require("./db.js");

// constructor

const Category = function(category){
    this.title = category.title;
    this.icon = category.icon;
};

Category.create = (newCategoryItem, result) => {
    sql.query("INSERT INTO categories SET ?", newCategoryItem, (err,res) => {
        if(err){
            console.log("Error: ", err);
            result(err,null);
            return;
        }

        console.log("Category created: ", {id: res.insertId, ...newCategoryItem})
        result(null, {id: res.insertId, ...newCategoryItem});
    });
};

Category.getProductsById = (id, result) => {
    sql.query(`SELECT * FROM shop LEFT JOIN categories ON ${id} = ${id}`, (err,res)=>{
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
          }
        
          if (res.length) {
            console.log("found shop item: ", res[0]);
            result(null, res[0]);
            return;
          }

           // not found Customer with the id
      result({ kind: "not_found" }, null);

    });
}

Category.getAll = result => {
    sql.query("SELECT * FROM categories", (err,res) => {
        if(err){
            console.log("error: ", err);
            result(null,err);
            return;
        }

        console.log("cats: ",res);
        result(null, res);
    });
};

Category.updateById = (id, category, result) => {
    sql.query("UPDATE categories SET title = ?, icon = ? WHERE id = ?", 
    [category.title, category.icon, id],
    (err,res)=>{
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }

        if(res.affectedRow == 0){
            result({kind: "not_found"}, null);
            return;
        }

        console.log("updated category: ", {id: id, ...category});
        result(null, {id:id, ...category});
    }
    );
};

Category.remove = (id, result) => {
    sql.query("DELETE FROM categories WHERE id = ?", id, (err,res) => {
        if(err){
            console.log("err: ", err);
            result(null, err);
            return;
        }

        if(res.affectedRow == 0){
            result({kind: "not_found"}, null);
            return;
        }

        console.log("deleted category with id: ", id);
        result(null,res);
    });
}

module.exports = Category;