const sql = require("./db.js");

const User = function (user) {
    this.username = user.username;
    this.name = user.name;
    this.surname = user.surname;
    this.email = user.email;
    this.avatar = user.avatar;
    this.locale = user.locale;
    this.token = user.token;
    this.created_at = new Date();
};


User.create = (newUser, result, err) => {

    const user = newUser
    //Check, if user exists
    sql.query("SELECT * FROM users WHERE email = ?", user.email, (err, rows, req, res)=>{

        const email = user.email;
        const emailDb = rows[0] ? rows[0].email : ''

        // console.log(`Email in payload: ${email}\nEmail in DB: ${emailDb}`)

        if(email == emailDb){
            sql.query("SELECT * FROM users WHERE email = ?", email, (err,res)=>{
                if(err){
                    result(err,null);
                }
                
                result(null,res);
            })
        }
        else if(emailDb == ''){
            sql.query("INSERT INTO users SET = ?", user, (err,res)=>{
                if(err){
                    result(err,null);
                }

                result(null, {id: res.insertId, ...newUser});
            })
        }

        result(null, res);
    })

    if(err){
        console.log(err)
    }


}

// User.create = (newUser, result) => {
//     sql.query(`SELECT * FROM users WHERE email = ?`, newUser.email, (err, rows, req, res) => {
//         if (err) {
//             sql.query("INSERT INTO users SET ?", newUser, (err, res) => {
//                 console.log("User created: ", { id: res.insertId, ...newUser });
//                 result(null, { id: res.insertId, ...newUser });
//                 return res.status(500).send("Ебать-копать!: ", err.message)

//             });
//         }

//         const email = newUser.email

//         if (rows[0].email == email) {
//             sql.query("SELECT * FROM users WHERE email = ?", rows[0].email, (_err, res) => {
//                 console.log("Logged as: ", res)
//                 result(null, res);
//                 return;
//             })
//         }
//     });
// };


module.exports = User;
