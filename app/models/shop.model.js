const sql = require("./db.js");

//constructor

const Shop = function(shop){
    this.title = shop.title;
    this.description = shop.description;
    this.category_id = shop.category_id;
    this.picture = shop.picture;
    this.gallery_id = shop.gallery_id;
    this.price = shop.price;
    this.quantity = shop.quantity;
};

Shop.create = (newShopItem, result) => {
    sql.query("INSERT INTO shop SET ?", newShopItem, (err,res) => {
        if(err){
            console.log("Error: ", err);
            result(err,null);
            return;
        }

        console.log("Shop item created: ", {id: res.insertId, ...newShopItem});
        result(null, {id: res.insertId, ...newShopItem});

    });


};

Shop.getAll = result => {
    sql.query("SELECT * FROM shop", (err,res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
          }
      
          console.log("shop: ", res);
          result(null, res);
    });
};

Shop.findById = (shopItemId, result) => {
    sql.query(`SELECT * FROM shop WHERE id = ${shopItemId}`, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(err, null);
        return;
      }
  
      if (res.length) {
        console.log("found shop item: ", res[0]);
        result(null, res[0]);
        return;
      }
  
      // not found Customer with the id
      result({ kind: "not_found" }, null);
    });
  };


  Shop.updateById = (id, shop, result) => {
    sql.query("UPDATE shop SET title = ?, description = ?, category_id = ?, picture = ?, price = ?, gallery_id = ?, quantity = ? WHERE id = ?",
    [shop.title, shop.description, shop.category_id, shop.picture, shop.price, shop.gallery_id, shop.quantity, id],
    (err,res)=>{
        if (err){
            console.log("error: ", err);
            result(null,err);
            return;
        }

        if(res.affectedRow == 0){
            result({kind: "not_found"}, null);
            return;
        }

        console.log("updated customer: ", {id:  id, ...shop});
        result(null, {id:id, ...shop});
    }
    );
  };

  Shop.remove = (id, result) => {
    sql.query("DELETE FROM shop WHERE id = ?", id, (err, res) => {
        if(err){
            console.log("error: ", err);
            result(null, err);
            return;
        }

        if(res.affectedRow == 0){
            result({kind: "not_found"}, null);
            return;
        }

        console.log("deleted customer with id: ", id);
        result(null, res);
    });
  };
module.exports = Shop;