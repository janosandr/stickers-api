const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const path = require("path")


const app = express();

// parse requests of content-type: application/json
app.use(bodyParser.json());
app.use(cors());

// parse requests of content-type: application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// simple route
app.get("/", (req, res) => {
  res.json({ message: "Стикерошная API" });
});

app.get("/egg/2020", (req,res)=>{
  res.sendFile(path.join(__dirname+'/static/index.html'))
})

require("./app/routes/shop.routes.js")(app);
require("./app/routes/categories.routes.js")(app);
require("./app/routes/users.routes.js")(app);
require("./app/routes/order.routes.js")(app);
require("./app/routes/services.routes.js")(app);
require("./app/routes/sliders.routes.js")(app);

// set port, listen for requests
app.listen(3000, () => {
  console.log("Server is running on port 3000.");
});